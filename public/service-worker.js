if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service-worker.js')
        .then((reg) => {
            console.log('Registration succeeded');
        }).catch((error) => {
            console.log('Registration failed with ' + error);
        });
}
